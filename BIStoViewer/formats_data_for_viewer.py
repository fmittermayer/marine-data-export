import csv
import re
import pandas as pd
import shapely as sh
import datetime as dt
import numpy as np


def main():
    
    filepath = "IDs_6679_to_6883/"
    filename = "samples_export_(IDs_6679_to_6883)"
    
    #öffnen der Ursprungsdatei
    with open(filepath + filename + ".csv", "r") as file:
        reader = csv.DictReader(file)    
        
        vals = list(reader)  #Liste der Dictionaries(Zeilen) mit header als Keys
        
        no_empty_cols_vals = delete_empty_cols(vals)
        
        formatted_header_vals = formate_all_header(no_empty_cols_vals)
        
        no_personal_info_vals = delete_personal_infos(formatted_header_vals) 
  
        combined_cols_vals = combine_cols(no_personal_info_vals)
        
        ## change/add here tuples of headernames, which shall be changed: ("old","new")
        rename_list = [("gear","device"),
                       ("gear_configuration","device_configuration"),
                       ("campaign","expedition"),
                       ("sampling_depth","z_value"),
                       ("sampling_depth_start","z_value_start"),
                       ("sampling_depth_stop","z_value_end"),
                       ("water_depth","elevation"),
                       ("water_depth_start","elevation_start"),
                       ("water_depth_stop","elevation_end"),
                       ("bottom_depth","elevation")]
        renamed_cols_vals = rename_cols(combined_cols_vals, rename_list)

        header_list = []
        if renamed_cols_vals:
            for key in renamed_cols_vals[0].keys():
                header_list.append(key)
            
    #speichert csv file mit formatierter Tabelle, aber allen Daten als strings
    with open(filename + "_strings.sdi.csv", "w", newline='') as file:
        obj = csv.DictWriter(file, header_list)
        obj.writeheader()
        obj.writerows(renamed_cols_vals)
        
        
    #lädt Daten nochmal ein, um Datentypen zu erhalten und abspeichern zu können:
    string_vals = pd.read_csv(filename + "_strings.sdi.csv", delimiter=",")
    
    changed_dtype_vals = change_data_types_into_POINT(string_vals) #ändert geometry Daten
    
    changed_dtype_vals2 = change_data_type_into_datetime(changed_dtype_vals) #ändert DateTime Daten
    
    #speichert endgültiges csv file mit Datentabelle
    changed_dtype_vals2.to_csv(filename + ".sdi.csv", index = False, sep = '\t')
    
    #speichert Datentyp aller Spalten in type_list
    type_list = find_dtype_of_cols(changed_dtype_vals2)
    

    #erstellt Liste mit den Datentypen zum Exportieren
    header_list = changed_dtype_vals2.columns.values.tolist()
    type_in_string_list = []
    for ty in type_list:
        if ty == str:
            type_in_string_list.append("String")
        elif ty == np.float64:
            type_in_string_list.append("Real")
        elif ty == np.int64:
            type_in_string_list.append("Integer")
        elif ty == bool:
            type_in_string_list.append("Integer(Boolean)")
        elif ty == dt.datetime:
            type_in_string_list.append("DateTime")
        elif ty == sh.Point or ty == sh.LineString:
            type_in_string_list.append("WKT")     
    
    #vorhandene _layer_def.csv (Headernamen) einladen:
    with open("_layer_def.csv", "r") as file_h:
        reader_h = csv.DictReader(file_h, delimiter = '\t')
        header_list_h = reader_h.fieldnames
    
    #vorhandene _layer_def.csvt (Datentypen) einladen:
    with open("_layer_def.csvt", "r") as file_ht:
        reader_ht = csv.DictReader(file_ht, delimiter = ',')
        type_list_ht = reader_ht.fieldnames
    
    #header, die noch nicht im layer_def enthalten sind, werden angehängt (+dtypes)
    for i,header in enumerate(header_list):
        if header not in header_list_h:
            header_list_h.append(header)
            type_list_ht.append(type_in_string_list[i])
        
    #neue headerlist
    # bis 'bis hier' entkommentieren, um neue layer_def zu erstellen (Absatz darüber auch)
    # header_file_name = filename + "_headerlist"
    # with open(header_file_name + ".csv", "w", newline='') as file:
    #     writer = csv.writer(file, delimiter='\t')
    #     writer.writerow(header_list)
    #bis hier
    with open("_layer_def.csv", "w", newline='') as file:    
        writer = csv.writer(file, delimiter='\t')
        writer.writerow(header_list_h)
        
    #neue dtypelist 
    # bis 'bis hier' entkommentieren, um neue layer_def zu erstellen (Absatz 2 darüber auch)
    # header_type_file_name = filename + "_headertypelist"
    # with open(header_type_file_name + ".csvt", "w", newline='') as file: 
    #     writer = csv.writer(file)
    #     writer.writerow(type_in_string_list) 
    # #bis hier
    with open("_layer_def.csvt", "w", newline='') as file:    
        writer = csv.writer(file)
        writer.writerow(type_list_ht) 
        

def delete_empty_cols(input_list):
    """
    deletes detected empty columns in a list of dictionaries.

    Parameters
    ----------
    input_list : list of dictionaries
        Elemente=Zeilen, keys=header.

    Returns
    -------
    output_list : list of dictionaries
        Elemente=Zeilen(ohne gelöschte Spalten), keys=header.

    """
    output_list = input_list + []
    header_list = []
    if input_list:
        for key in input_list[0].keys():
            header_list.append(key)
    dict_empty = detect_empty_cols(header_list, input_list)
    
    #delete empty columns
    for header in header_list:
        # print(dict_empty[header])
        if not dict_empty[header]:
            for idx,_ in enumerate(input_list):
                del output_list[idx][header]
    return output_list


def detect_empty_cols(headers, list_dicts):
    """
    detects empty columns in a list of dictionaries.

    Parameters
    ----------
    headers : list
        keys of dictionary.
    list_dicts : list of dictionaries
        original csv data.

    Returns
    -------
    dict_empty : dictionary
        Keys: headers, values: bool (False == empty column)

    """
    dict_empty = dict(zip(headers, [False]*len(headers)))  #erstellt Dictionary mit headers als keys und False als values
    for header in headers:
        for idx2,_ in enumerate(list_dicts):
            if list_dicts[idx2][header]:
                dict_empty[header] = True
    #print(dict_empty)
    return dict_empty


def formate_all_header(input_list):
    """
    Formatiert alle header-Namen. Schreibt große Buchstaben zu kleinen und 
    Leerzeichen zu Unterstrichen um.

    Parameters
    ----------
    input_list : list of dictionaries
        Elemente=Zeilen, keys=header.

    Returns
    -------
    output_list : list of dictionaries
        Elemente=Zeilen, keys=neue header.

    """
    output_list = input_list + []
    header_list = []
    if input_list:
        for key in input_list[0].keys():
            header_list.append(key)
    #print(header_list)
    for idx,dictionary in enumerate(input_list):
        for header in header_list:
            output_list[idx][formate_header(str(header))] = dictionary[header]
            del output_list[idx][header]
    return output_list


def formate_header(input):
    """
    orders headline, replaces " " with "_" and makes it lowercase

    Args:
        input (string): fieldname

    Returns:
        string: ordered fieldname
    """
    output = ""
    for idx,char in enumerate(input):
        if char.isupper() and idx != 0:
            if input[idx-1].islower():
                output += "_"
        output += char
    return output.lower().replace(" ", "_")

def delete_personal_infos(input_list):
    """
    Deletes every column with personal infos in.

    Parameters
    ----------
    input_list : list of dictionaries
        Elemente=Zeilen, keys=header.

    Returns
    -------
    output_list : list of dictionaries
        Elemente=Zeilen, keys=neue header.

    """
    output_list = input_list + []
    header_list = []
    if input_list:
        for key in input_list[0].keys():
            header_list.append(key)
    #add here keys of personal columns, which shall be deleted
    pers_header = ["sampling_person","pi","storage_id","person","storage_container_label"] #these cols are going to be deleted
    
    for header in header_list:
        if header in pers_header:
            for idx,_ in enumerate(input_list):
                del output_list[idx][header]
    return output_list


def combine_cols(input_list):
    """
    Combines date column with time column to date_time
    and lat column with lon column to geometry.

    Parameters
    ----------
    input_list : list of dictionaries
        Elemente=Zeilen, keys=header.

    Returns
    -------
    output_list : list of dictionaries
        Elemente=Zeilen, keys=neue header.

    """
    output_list = input_list + []
    header_list = []
    if input_list:
        for key in input_list[0].keys():
            header_list.append(key)

    
    for idx_h,header in enumerate(header_list):
            #put together date start and time start in date_time_start
            if header == "date_start" and "time_start" in header_list:    #zum abfangen bei anderen Schreibweisen/nicht vorhandenen Spalten
                for idx,dictionary in enumerate(input_list):
                    if input_list[idx]["date_start"] and input_list[idx]["time_start"]:
                        output_list[idx]['date_time_start'] = input_list[idx]["date_start"] + "T" + input_list[idx]["time_start"]
                    else:
                        output_list[idx]['date_time_start'] = ""
                    del output_list[idx]['date_start']
                    del output_list[idx]['time_start']
            if header == "date_stop" and "time_stop" in header_list:
                for idx,dictionary in enumerate(input_list):
                    if input_list[idx]["date_stop"] and input_list[idx]["time_stop"]:
                        output_list[idx]['date_time_end'] = input_list[idx]["date_stop"] + "T" + input_list[idx]["time_stop"]
                    else:
                        output_list[idx]['date_time_start'] = ""
                    del output_list[idx]['date_stop']
                    del output_list[idx]['time_stop']       
                    
            #put lat,lon together in geometry
            if header == "latitude" and "longitude" in header_list:
                for idx,dictionary in enumerate(input_list):
                    if input_list[idx]["longitude"] and input_list[idx]["latitude"]: 
                        output_list[idx]['geometry'] = "POINT(" + input_list[idx]["longitude"] +" "+ input_list[idx]["latitude"] + ")"
                    else:
                        output_list[idx]['date_time_start'] = ""
                    del output_list[idx]['latitude']
                    del output_list[idx]['longitude']
            if header == "latitude_start" and "longitude_start" in header_list:
                for idx,dictionary in enumerate(input_list):
                    if input_list[idx]["longitude_start"] and input_list[idx]["latitude_start"]:    
                        output_list[idx]['geometry_start'] = "POINT(" + input_list[idx]["longitude_start"] +" "+ input_list[idx]["latitude_start"] + ")"
                    else:
                        output_list[idx]['date_time_start'] = ""
                    del output_list[idx]['latitude_start']
                    del output_list[idx]['longitude_start']
            if header == "latitude_stop" and "longitude_stop" in header_list:
                for idx,dictionary in enumerate(input_list):
                    if input_list[idx]["longitude_stop"] and input_list[idx]["latitude_stop"]:
                        output_list[idx]['geometry_end'] = "POINT(" + input_list[idx]["longitude_stop"] +" "+ input_list[idx]["latitude_stop"] + ")"
                    else:
                        output_list[idx]['date_time_start'] = ""
                    del output_list[idx]['latitude_stop']
                    del output_list[idx]['longitude_stop']
                    
    return output_list


def rename_cols(input_list, rename_list):
    """
    Renames columns with encouraged vocabulary.

    Parameters
    ----------
    input_list : list of dictionaries
        elements=rows, keys=header.
    rename_list : list of tuples
        defines which and how headers are to be renamed: [("old","new"),...]

    Returns
    -------
    output_list : list of dictionaries
        Elemente=Zeilen, keys=neue header.

    """
    output_list = input_list + []
    header_list = []
    if input_list:
        for key in input_list[0].keys():
            header_list.append(key)
        
    for old, new in rename_list:
        if old in header_list:
            for idx,dictionary in enumerate(input_list):
                output_list[idx][new] = dictionary[old]
                del output_list[idx][old]
    return output_list    

  
def change_data_types_into_POINT(input_frame):
    """
    Elements of geometry columns get the datatype "sh.POINT"

    Parameters
    ----------
    input_frame : pd.DataFrame
        old Dataframe.

    Returns
    -------
    output_frame : pd.DataFrame
        new pd.DataFrame.

    """
    output_frame = input_frame.copy()
    header_list = input_frame.columns.values.tolist()
    if 'geometry' in header_list:
        for i,_ in input_frame.iterrows():
            point_val = sh.from_wkt(str(input_frame._get_value(i, 'geometry')))
            output_frame = output_frame.replace(input_frame._get_value(i, 'geometry'), point_val)
    if 'geometry_start' in header_list:
        for i,_ in input_frame.iterrows():
            point_val = sh.from_wkt(str(input_frame._get_value(i, 'geometry_start')))
            output_frame = output_frame.replace(input_frame._get_value(i, 'geometry_start'), point_val)
    if 'geometry_end' in header_list:
        for i,_ in input_frame.iterrows():
            point_val = sh.from_wkt(str(input_frame._get_value(i, 'geometry_end')))
            output_frame = output_frame.replace(input_frame._get_value(i, 'geometry_end'), point_val)
                       
    #if two geometry columns exist, they get formatted into LineString (geometry_start, geometry_end)
    #columns geometry_end and geometry_start gets deleted, a new geometry column appended
    if 'geometry_start' and 'geometry_end' in header_list:
        geometry_list = []
        for i,_ in input_frame.iterrows():          
            point_val_start = sh.from_wkt(str(input_frame._get_value(i, 'geometry_start')))
            point_val_end = sh.from_wkt(str(input_frame._get_value(i, 'geometry_end')))
            line_string_val = sh.LineString([point_val_start, point_val_end])
            geometry_list.append(line_string_val)
        output_frame = output_frame.drop(columns=['geometry_start'])
        output_frame = output_frame.drop(columns=['geometry_end'])
        output_frame['geometry'] = geometry_list
        
    return output_frame


def change_data_type_into_datetime(input_frame):
    """
    Elements of date_time columns get the datatype "dt.datetime"

    Parameters
    ----------
    input_frame : pd.DataFrame
        old Dataframe.

    Returns
    -------
    output_frame : pd.DataFrame
        new pd.DataFrame.

    """
    output_frame = input_frame
    header_list = input_frame.columns.values.tolist()
    date_header = ['date_time', 'date_time_start', 'date_time_end']
    for i,_ in input_frame.iterrows():
        for header in date_header:
            if header in header_list:
                date = input_frame._get_value(i, header)
                date_list = re.split(r'-|T|:', date)
                year = int(date_list[0])
                month = int(date_list[1])
                day = int(date_list[2])
                hour = int(date_list[3])
                minute = int(date_list[4])
                sec = int(date_list[5])
                datetime = dt.datetime(year, month, day, hour, minute, sec)   # -> liefert datetime, aber ohne 'T'
                #iso_date = datetime.isoformat()    # -> liefert string, aber mit 'T' in der Mitte
                output_frame = output_frame.replace(input_frame._get_value(i, header), datetime)        
    return output_frame


def find_dtype_of_cols(input_frame):
    """
    Saves data types of all columns in return list.

    Parameters
    ----------
    input_frame : pd.DataFrame
        old Dataframe.

    Returns
    -------
    dtype : list
        contains all data types of the columns (same order as the headers).

    """
    header_list = input_frame.columns.values.tolist()
    dtype = []
    for header in header_list:
        for i,_ in input_frame.iterrows():    
            if input_frame._get_value(i, header) is not np.nan: #seeks first not_nan value in col to get the dtype
                dtype.append(type(input_frame._get_value(i, header)))
                break
    
        # falls nan-werte noch in dtype der Spalte umgewandelt werden müssen, hier der erste Versuch
        # for idx,_ in enumerate(input_frame[header]):   # nochmal von vorn um alle ersten leeren Zellen zu formatieren
        #     if dtype == str:    ##hier müssen noch die anderen dtypen als elif hin
        #         #print('truee')
        #         if type(input_frame._get_value(idx, header)) != str:
        #             print('true')
                            
        #             output_dict["output_"+header][idx] = ""
        #             break
                    
                # print(type(input_frame._get_value(idx, header)))
                # if input_frame._get_value(idx, header) == np.nan: 
                #     print('true')
                #     output_dict["output_"+header][idx] = ""
            #elif dtype == float oder so
    #print(output_dict['output_ecotaxa_url'][0])
    return (dtype)
           
    
main()