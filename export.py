#     Export Sample Template to O2A specification
#     Copyright (C) 2022  Jakob Eckstein
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import json
import re
import pandas as pd
from dataclasses import dataclass, asdict, field
from typing import List

BASE_NAME = "/home/jeckstein/projects/mareHUB/marine-data-export/DEMO_export_samples"

METADATA_VERSION = "2.0"

COLUMN_HEADERS = [
"date_time_start",
"date_time_end",
"elevation [m]",
"z_value [m]",
"z_type",
"event_name",
"sample_id",
"contact_email",
"institute_responsible_for_sample_repository",
"taxon",
"type_of_device",
"ObsView",
"SeafloorView",
"sample_type",
"metadata_url",
"geometry"  # This must be the last column in the GeoCSV File.
]

VALID_Z_TYPES = [
"DEPTH, sediment/rock",
"DEPTH, water",
"DEPTH, ice/snow",
"DEPTH, soil",
"ELEVATION",
"ALTITUDE",
"HEIGHT above ground"
]

def exclude_empty_fields(data):
    """
    data: An instance of a dataclass
    return: A dictionary containing only the fields in data that are
    not None or []
    """
    return {
    key: value for key, value in data if value is not None and value # "and value" evaluates to false for value == []
    }

@dataclass
class Meta:
    """The meta information in metadata .json files"""
    pi_name: str = None
    pi_email: str = None
    pi_url: str = None
    pi_orcid: str = None
    comment: str = None
    citation: str = None
    project: str = None
    license: str = None
    metadata_url: str = None
    data_url: str = None
    sop_url: str = None

@dataclass
class Base:
    name: str
    alias: str = None
    uri: str = None
    meta: Meta = None

@dataclass
class Event(Base):
    expedition: str = None
    platform: str = None
    device: str = None

@dataclass
class Parameter(Base):
    unit: str = None
    method: str = None

@dataclass
class Metadata:
    events: List[Event] = field(default_factory = list)
    version: str = METADATA_VERSION
    parameters: List[Parameter] = field(default_factory = list)
    expeditions: List[Base] = field(default_factory = list)
    platforms: List[Base] = field(default_factory = list)
    projects: List[Base] = field(default_factory = list)
    meta: Meta = None
    def add_event(self, event):
        if event not in self.events:
            self.events.append(event)
    def add_parameter(self, parameter):
        if parameter not in self.parameters:
            self.parameters.append(parameter)
    def add_expedition(self, expedition):
        if expedition not in self.expeditions:
            self.expeditions.append(expedition)
    def add_platform(self, platform):
        if platform not in self.platforms:
            self.platforms.append(platform)
    def add_projects(self, projects):
        if projects not in self.projects:
            self.projects.append(projects)

def do_export(file, input_dir, output_dir):
    if file.endswith(".xlsx"):
        data = pd.read_excel(os.path.join(input_dir, file),
        skiprows=[1], # Comments in second line (0-indexed -> index 1)
        nrows=11, # For testing only, since there is a "Legend" in the template
        )

        metadata = Metadata()
        csv_data = []   # A List for creating the dataframe that contains the data for the GeoCSV file

        for index, row in data.iterrows():
            # Check if current row meets the specifications. Print a message and skip the row if the specifications are not met:
            # 1. Check for the correct time format.
            if re.search("[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]T[012][0-9]:[0-5][0-9]:[0-5][0-9]", str(row["date_time_start"])) is None:
                print("Row {}: Invalid 'date_time_start': '{}'. This field is mandatory and has to be formated as yyyy-MM-dd'T'HH:mm:ss".format(index, row["date_time_start"]))
                continue
            # 2. Check that z_type is specified when z_value is given
            if (not pd.isna(row["z_value [m]"])) and (pd.isna(row["z_type"])):
                print("Row {}: 'z_type' is mandatory, if 'z_value [m]' is given.".format(index))
                continue
            # 3. Check that z_type is a Valid Type following the PANGAEA specs.
            if not( row["z_type"] in VALID_Z_TYPES or pd.isna(row["z_type"])):
                print("Row:{}: Invalid 'z_type': {}. This field should be empty or one of {}".format(index, row[z_type], VALID_Z_TYPES))
                continue
            # 4. Check that the Event Name is not empty
            if pd.isna(row["event_name"]):
                print("Row: {}: 'event_name' is mandatory and can not be empty.".format(index))
                continue
            # 5. Check if the Geometry is specified and valid
            if re.search("(POINT\(-?[0-9]+\.[0-9]+ -?[0-9]+\.[0-9]+\))", row["geometry"]) is None:
                print("Row: {}: Invalid 'geometry': {}. This Field is mandatory and should contain a point in WKT-notation, e.g. POINT(0.0 10.42)".format(index, row["geometry"]))
                continue

            # Add sample data to csv_data
            sample = []
            for header in COLUMN_HEADERS:
                sample.append(row[header])
            csv_data.append(sample)

            # Add Metadata
            metadata.add_event(Event(name = row["event_name"], expedition = row["expedition"], platform = row["platform"]))
            metadata.add_expedition(Base(name=row["expedition"]))
            metadata.add_platform(Base(name=row["platform"]))

        output_file = os.path.join(output_dir, file[0:-5]) # file[0:-5] gives the name of the file without ".xlsx"
        # Create GeoCSV
        df = pd.DataFrame(csv_data, columns = COLUMN_HEADERS)
        df.to_csv("{}.sdi.tab".format(output_file), sep="\t")

        # Create Metadata JSON
        with open("{}.sdi.meta.json".format(output_file), "w") as out:
            json.dump(asdict(metadata, dict_factory = exclude_empty_fields), out)
    else:
        print("Ignoring non-xlsx file: {}".format(path))

def main():
    base_path = os.path.dirname(__file__)
    input_dir = os.path.join(base_path, "input")
    output_dir = os.path.join(base_path, "output")

    for file in os.listdir(input_dir):
        do_export(file, input_dir, output_dir)

if __name__ == "__main__":
    main()
